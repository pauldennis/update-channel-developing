set -e

git submodule update --init --recursive

pushd input-channels/niv-sources

  git status
  git fetch
  git checkout main

popd

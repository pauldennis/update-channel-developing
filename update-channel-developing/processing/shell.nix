{ sources ? import ./nix/sources.nix
, pkgs ? import sources.nixpkgs {}
, glibcLocales ? pkgs.glibcLocales

}:

pkgs.mkShell {
  buildInputs = [
    pkgs.exa
    pkgs.wabt
    pkgs.git
    pkgs.cacert
  ];

  shellHook = ''
    echo this is a nix shell; alias l="ls -al"
  '';

  # excruciating unicode and whatnot
  LOCALE_ARCHIVE="${glibcLocales}/lib/locale/locale-archive";
  LANG="en_GB.UTF8";


}

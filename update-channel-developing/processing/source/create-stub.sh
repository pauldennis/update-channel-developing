set -e

# user arguments

copyright_token=$1

if [ -z "$1" ]
  then
    echo "No argument copyright_token"
    exit 1
fi

if [ $# -eq 1 ]
  then
    :
  else
    echo "not enough or too much arguments"
    exit 2
fi


# well-known arguments

input_channels="input-channels"
processing="processing"
output_channels="output-channels"


# repository has correct name

root_directory_name=$(basename $(git rev-parse --show-toplevel))
repository_name=$root_directory_name


# we are in root directory

root_directory=$(git rev-parse --show-toplevel)
current_directory=$(pwd)

if [ "$root_directory" = "$current_directory" ]; then
    echo "we are in root. ok"
else
    echo "script is supposed to be executed in the root of a repository"
    exit 4
fi



dump_fetch_dependencies_sh()
{
  filename=$1

  echo -n "" > $filename

  echo "set -e" >> $filename
  echo "" >> $filename
  echo "git submodule update --init --recursive" >> $filename
  echo "" >> $filename
  echo "pushd input-channels/niv-sources" >> $filename
  echo "" >> $filename
  echo "  git status" >> $filename
  echo "  git fetch" >> $filename
  echo "  git reset --hard origin/main" >> $filename
  echo "" >> $filename
  echo "popd" >> $filename
}

# create folder with the same name as the repository

mkdir $repository_name -p
pushd $repository_name
  mkdir $input_channels -p
  pushd $input_channels
    git submodule add https://gitlab.com/pauldennis/niv-sources.git || true
  popd

  mkdir $processing -p
  pushd $processing
  popd

  mkdir $output_channels -p
  pushd $output_channels
  popd

  dump_fetch_dependencies_sh "fetch-dependencies.sh"
popd

